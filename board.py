import sqlite3
from sqlite3 import Error
import datetime

class GameDatabase:
    def __init__(self):
        self.connect()

    def connect(self):
        try:
            self.connection = sqlite3.connect('tic_tac_toe.db')
        except Error as e:
            print(e)

    def table_exists(self):
        cursor = self.connection.cursor()
        cursor.execute(''' select count(name) from sqlite_master where type='table' and name='tic_tac_toe' ''')
        if cursor.fetchone()[0] == 1 : 
            return True
        return False

    def find_games_played(self):
        cursor = self.connection.cursor()
        cursor.execute(''' select count(*) from tic_tac_toe ''')
        return cursor.fetchone()[0]

    def create_table(self):
        cursor = self.connection.cursor()
        cursor.execute(''' create table tic_tac_toe (
            game_id INTEGER PRIMARY KEY AUTOINCREMENT,
            game_date TIMESTAMP,
            game_winner VARCHAR(10) NOT NULL,
            game_board TEXT NOT NULL
        );
        ''')

    def insert_results(self, winner, board):
        try:
            cursor = self.connection.cursor()
            sqlite_insert_with_param = """INSERT INTO 'tic_tac_toe'
                            ('game_date', 'game_winner', 'game_board') 
                            VALUES (?, ?, ?);"""
            data = (datetime.datetime.now(), winner, board) 
            cursor.execute(sqlite_insert_with_param, data)
            self.connection.commit()
        except Error as e:
            print(e)


    def disconnect(self):
        if self.connect is not None:
            self.connection.close()

class TicTacToe:
    def __init__(self):
        self.db = GameDatabase()
        self.db.connect()
        if not self.db.table_exists():
            self.db.create_table()
        else:
            print('You had ' + str(self.db.find_games_played()) + ' game(s) before.')

        self.board = []

    def create_board(self):
        for i in range(3):
            row = []
            for j in range(3):
                row.append('-')
            self.board.append(row)

    def print_board(self):
        for row in self.board:
            print row

    def player_mark(self, player):
        return 'X' if player == 1 else 'O';

    def swap_players(self, player):
        return 2 if player == 1 else 1;

    def make_move(self, row, col, player):
        if not self.board[row][col] == '-':
            raise ValueError('Board [{0}][{1}] has already been selected. Please make a move somewhere else on the board.'.format(row, col))
        else:
            self.board[row][col] = self.player_mark(player)

    def check_board(self, row, col):

        if self.board[row][col] != '-':
            print 'Board [{0}][{1}] has already been selected. Please make a move somewhere else on the board.'.format(row, col)

        return True

    def is_player_win(self, player):
        win = None
        n = len(self.board)

        # Rows
        for i in range(n):
            win = True
            for j in range(n):
                if self.board[i][j] != self.player_mark(player):
                    win = False
                    break
            if win:
                return win

        # Columns
        for i in range(n):
            win = True
            for j in range(n):
                if self.board[j][i] != self.player_mark(player):
                    win = False
                    break
            if win:
                return win

        # Diagonals
        win = True
        for i in range(n):
            if self.board[i][i] != self.player_mark(player):
                win = False
                break
        if win:
            return win

        win = True
        for i in range(n):
            if self.board[i][n - 1 - i] != self.player_mark(player):
                win = False
                break
        if win:
            return win
        return False

        for row in self.board:
            for item in row:
                if item == '-':
                    return False
        return True

    def is_draw(self):
        for row in self.board:
            for item in row:
                if item == '-':
                    return False
        return True


    def start(self):
        self.create_board()
        player = 1
        while True:
            self.print_board()
            print 'Player {0}, make your move'.format(player)
            row = int(input('Enter row (0-2): '))
            col = int(input('Enter col (0-2: '))

            try:
                self.make_move(row, col, player)
            except ValueError as e:
                print e
                continue
            except IndexError:
                print 'Invalid row or column. Please select row / column between values 0 and 2.'
                continue

            if self.is_player_win(player):
                self.db.insert_results(player, '\n'.join('\t'.join(y) for y in self.board))
                print 'Player {0} wins! Game Over!'.format(player)
                self.db.disconnect()
                break

            if self.is_draw():
                self.db.insert_results('draw', '\n'.join('\t'.join(y) for y in self.board))
                print 'This is a draw!'
                self.db.disconnect()
                break

            player = self.swap_players(player)

if __name__ == '__main__':
    ttt = TicTacToe()
    ttt.start()
